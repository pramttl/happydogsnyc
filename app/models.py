from __future__ import unicode_literals

from django.core.exceptions import ValidationError
from django.db import models
from django.dispatch import receiver
from django.db.models.signals import pre_save, post_save


class Dog(models.Model):

    first_name = models.CharField(max_length=256, null=False, blank=False)
    last_name = models.CharField(max_length=256, null=True, blank=True)

    def __unicode__(self):

        if self.last_name:
            return "%s %s" % (self.first_name, self.last_name)
        else:
            return self.first_name

    class Meta:
        unique_together = ("first_name", "last_name")


class Visit(models.Model):
    """
    Record each boarding visit.
    """
    start = models.DateField()
    end = models.DateField()
    dog = models.ForeignKey(Dog)

    def __unicode__(self):
        return "%s: %s - %s"%(self.dog, str(self.start), str(self.end))

    def clean(self):
        # Case 1: Check if end > start
        start = self.start
        end = self.end
        dog = self.dog
        if start >= end:
            raise ValidationError('End date must be after start date')

        # Case 2: Check no overlapping dates.
        overlaps = Visit.objects.filter(end__gte=start, start__lte=end, dog=dog).count()
        if overlaps > 0:
            raise ValidationError('New visit is overlapping with old visit.')


# Do a full clean whenever Dog model is saved
@receiver(pre_save, sender=Dog)
def pre_save_handler(sender, instance, *args, **kwargs):
    instance.full_clean()


# Do a full clean whenever Visit model is saved
@receiver(pre_save, sender=Visit)
def pre_save_handler(sender, instance, *args, **kwargs):
    instance.full_clean()
