from collections import OrderedDict
from datetime import date, timedelta as td
import holidays
import factory
import factory.fuzzy
from models import *
import random


def weighted_pick(d):
    '''
    Takes in a dictionary with keys being any object, specially strings; associates the value as the corresponding
    weight for that object and selects a KEY randomly with the weighted probality constraints.
    '''
    r = random.uniform(0, sum(d.itervalues()))
    s = 0.0
    for k, w in d.iteritems():
        s += w
        if r < s: return k
    return k

d1 = date(2016, 1, 1)       # Start of year
d2 = date(2016, 12, 31)     # End of year
delta = d2 - d1
us_holidays = holidays.UnitedStates()

# Creating a dictionary that will store a date and it's weight
# Weight is the probability that a dog will board on that day.
dates_2016 = OrderedDict()

for i in range(delta.days + 1):
    curr_date = d1 + td(days=i)

    curr_weight = dates_2016.get(curr_date)

    if not curr_weight:
        # Normal weight = 1. All other weights are relative to this
        dates_2016[curr_date] = 1

    curr_weekday = curr_date.weekday()
    if curr_weekday == 4 or curr_weekday == 5:
        # Fri or Sat

        # 5 times more likely for dog to board on Fri or Sat
        dates_2016[curr_date] *= 5

    if curr_date in us_holidays:

        curr_date_minus_two = curr_date - td(days=2)
        curr_date_plus_two = curr_date + td(days=2)

        if curr_date_minus_two.year == 2016:
            dates_2016[curr_date_minus_two] *= 3

        if curr_date_plus_two.year == 2016:
            # The weight for a date ahead of curr_date would not be defined yet
            dates_2016[curr_date_plus_two] = 3


def get_random_start_date(dog):
    start = weighted_pick(dates_2016)
    return start


def get_random_end_date(dog, start, retry=0):
    """
    Takes start date and generates an end_date after start_date but on/before Dec 31, 2016
    """
    start_date_index = dates_2016.keys().index(start)
    ndays = len(dates_2016.keys())
    end_index = random.choice(range(start_date_index+1, ndays))
    end = dates_2016.keys()[end_index]

    overlaps = Visit.objects.filter(end__gte=start, start__lte=end, dog=dog).count()
    if overlaps:
        # If there is an overlap, retry up to 5 times.
        if retry == 5:
            raise Exception('Unable to get non-overlapping dates in VisitFactory')
        retry += 1
        return get_random_end_date(dog, start, retry=retry)

    else:
        return end

################# Factories #################

class DogFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Dog
        django_get_or_create = ('first_name', 'last_name',)

    first_name = factory.Faker('first_name')
    last_name = factory.Faker('last_name')


class VisitFactory(factory.django.DjangoModelFactory):

    class Meta:
        model = Visit
        django_get_or_create = ('start', 'end', 'dog')

    start = factory.LazyAttribute(lambda visit: get_random_start_date(visit.dog))
    end = factory.LazyAttribute(lambda visit: get_random_end_date(visit.dog, visit.start))
    dog = factory.SubFactory(DogFactory)
