from django.db import IntegrityError
from django.core.exceptions import ValidationError
from django.test import TestCase, RequestFactory

# Create your tests here.
from django.urls import reverse

from .models import *
from .views import *
from .factories import *

# Ref. http://stackoverflow.com/questions/33645780/how-to-unit-test-methods-inside-djangos-class-based-views
def setup_view(view, request, *args, **kwargs):
    """Mimic ``as_view()``, but returns view instance.
    Use this function to get view instances on which you can run unit tests,
    by testing specific methods."""

    view.request = request
    view.args = args
    view.kwargs = kwargs
    return view


class ModelTests(TestCase):
    """
    This contains unit tests for models that test the model level constraints.
    """

    def setUp(self):
        dog = Dog.objects.create(first_name="Google", last_name="Doodle")
        Visit.objects.create(
            start=models.DateField().to_python('2016-05-01'),
            end=models.DateField().to_python('2016-06-15'),
            dog=dog,
        )

    def test_dog_duplicate_name_not_allowed(self):
        with self.assertRaises(ValidationError):
            Dog.objects.create(first_name="Google", last_name="Doodle")

    def test_dog_blank_first_name(self):
        with self.assertRaises(ValidationError):
            Dog.objects.create(last_name="Doodle")

    def test_visit_without_dog_not_allowed(self):
        with self.assertRaises(Dog.DoesNotExist):
            Visit.objects.create(
                start=models.DateField().to_python('2016-05-01'),
                end=models.DateField().to_python('2016-06-15'),
            )

    def test_no_visit_ends_before_start(self):
        with self.assertRaises(ValidationError):
            dog = Dog.objects.all()[0]
            Visit.objects.create(
                start=models.DateField().to_python('2016-06-15'),
                end=models.DateField().to_python('2016-05-01'),
                dog=dog,
            )

    def test_no_overlapping_visit(self):
        with self.assertRaises(ValidationError):
            dog = Dog.objects.all()[0]
            visit = Visit.objects.create(
                start=models.DateField().to_python('2016-06-10'),
                end=models.DateField().to_python('2016-07-12'),
                dog=dog,
            )

    def test_same_dog_can_have_multile_visits(self):
        dog = Dog.objects.get(first_name='Google', last_name='Doodle')
        visit = Visit.objects.create(
            start=models.DateField().to_python('2016-07-05'),
            end=models.DateField().to_python('2016-09-17'),
            dog=dog,
        )



class HomeViewTestCase(TestCase):
    """
    ViewTest: Testing the HomeView.
    Note: For testing views I believe it is most important to the the context data being passed to templates.
    I am not adding trivial test cases like checking response codes, etc and instead focusing on testing
    context variable(s).
    """

    def setUp(self):
        reset_db()
        # Notice: I've used the same function reset_db in the ResetDBView.
        # Unlike the RestDBView, using reset_db function here will run in the context of the temporary test database.

    def test_context_data(self):
        """
        Home.get_context_data() sets 'weeks' in context. weeks is an array of calendar weeks.
        Each week is a tuple, that contains the name of the weekday, the date and the number of dogs for that day.
        """
        # Setup request and view.
        request = RequestFactory().get('/home')
        view = HomeView()
        view = setup_view(view, request)

        context = view.get_context_data()

        total_number_of_dogs = Dog.objects.all().count()

        assert('weeks' in context)

        # Number of dogs on each day of the week should not exceed total number of dogs
        for week in context['weeks']:
            for day in week:
                if day:
                    ndogs = day[2]

                    # Number of dogs "in house" on any given day cannot be more than total number of dogs in DB.
                    self.assertLessEqual(ndogs, total_number_of_dogs)


class DayViewTestCase(TestCase):
    """
    ViewTest: Testing the DayView which prints a table containing, dog name, start date, end date
    """

    def setUp(self):
        reset_db()

    def test_context_data(self):
        """
        Day view defines 'visits' in the context data.
        """
        # Setup request and view.
        request = RequestFactory().get('/day/?d=2016-01-15')
        view = DayView()
        view = setup_view(view, request)
        context = view.get_context_data()

        assert('visits' in context)

        # Number of dogs on each day of the week should not exceed total number of dogs
        dogs = []
        for visit in context['visits']:
            dogs.append(visit.dog)

        if len(set(dogs)) < len(dogs):
            # Implies there must have been some duplicate dogs on the same day
            raise Exception('Same dog visiting more than once on the same day spotted (This should not be possible')
