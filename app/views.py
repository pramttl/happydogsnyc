from datetime import date, timedelta as td
from django.shortcuts import render
from django.http import HttpResponse
from django.views import View
from django.views.generic import TemplateView
from django.db import models
from .models import *
from .factories import *
import json
import random
from pprint import pprint


################### Utility Functions ###################

weekdays_dict = {
    0: 'Mon',
    1: 'Tue',
    2: 'Wed',
    3: 'Thurs',
    4: 'Fri',
    5: 'Sat',
    6: 'Sun',
}

def get_dates_in_range(d1, d2):
    """
    Get all dates between 2 dates including d1 and d2.
    """
    delta = d2 - d1
    result = []
    for i in range(delta.days + 1):
        result.append(d1 + td(days=i))
    return result

def group_date_series_by_week(dates_dict):
    """
    dates is a dict, where date is the key and value can be anything.
    Returns a list of tuples, where each tuple is of size 7 (one value for each day of week).
    """
    rows = []
    row = [None] * 7    # Represents a calendar week
    date_series = dates_dict.keys()
    len_date_series = len(date_series)

    for i, d in enumerate(date_series):
        weekday = d.weekday()
        row[weekday] = (weekdays_dict[weekday], d, dates_dict[d])

        if (weekday == 6) or (i == len_date_series-1):
            # End the week at Sunday or last date in given date series
            rows.append(row)
            row = [None] * 7    # Reinitialize row (i.e. calendar week)

    return rows


def reset_db():
    """
    This function deletes all existing Visit and Dog records and creates new fake data.
    Note: This function is used both in the RestDBView and in the tests.
    #XXX: This function should ideally be refactored to utils.py.
    """
    Visit.objects.all().delete()
    Dog.objects.all().delete()

    dogs = []
    for ndogs in range(10):
        dog = DogFactory()
        dogs.append(dog)

    for nvisits in range(100):
        try:
            VisitFactory(dog=random.choice(dogs))
        except:
            # Exception will happen if an attempt is made to add an overlapping visit.
            #Todo: Factories can be made smarter to generate visits without exceptions.
            pass

######################### Views #########################


class ResetDBView(View):
    """
    This view will be used to reset the database with new data. Note: I have not added any authorization to this view,
    so anyone can reset the db by simply visiting url corresponding to this view.
    """

    def get(self, request):
        try:
            reset_db()
            return HttpResponse("Database has been reinitialized with test Dog and Visit data.")
        except Exception:
            return HttpResponse("Reset db failed.", status=500)



class HomeView(TemplateView):
    """
    This page shows the following:
    - 2 input buttons to enter start and end dates.
    - Table (calendar) showing number of dogs on each date.

    If no date is provided start date is assumed to be Nov 1, 2016; and end date is assumed as Dec 3, 2016 as
    these were the example dates in the problem statement.
    """

    template_name = 'home.html'

    def get_context_data(self, **kwargs):

        start_str = self.request.GET.get('start', '2016-11-01')
        end_str = self.request.GET.get('end', '2016-12-03')

        start = models.DateField().to_python(start_str)
        end = models.DateField().to_python(end_str)

        intermediate_dates = get_dates_in_range(start, end)

        #Todo: Optimization: Create a new Date model to cache number of dogs for each date.
        # Alternatively key-value store can be used for caching.

        dogs_per_date = OrderedDict()
        for d in intermediate_dates:
            ndogs = Visit.objects.filter(start__lte=d, end__gt=d).count()
            dogs_per_date[d] = ndogs

        # weeks will contain one row per calendar week. Each row is a 3-tuple containing weekday index (0-6),
        weeks = group_date_series_by_week(dogs_per_date)
        return {'weeks': weeks}


class DayView(TemplateView):
    """
    This page returns: Dog name, Start Date, End Date
    """

    template_name = 'details.html'

    def get_context_data(self, **kwargs):
        d = self.request.GET.get('d')    # d is the date parameter for which this view returns the visit details
        if not d:
            raise Exception("A GET parameter 'd' must be provided in YYYY-MM-DD format")

        visits = Visit.objects.filter(start__lte=d, end__gt=d).order_by('start', 'end', 'dog__first_name', 'dog__last_name')
        return {'visits': visits}
